package com.bertoncelj.wildflysingletonservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import java.util.List;

/**
 * Starts or stops services on local server node.
 * List of services is obtained from {@link Deployer}.
 *
 * @author Rok Bertoncelj
 */
@Singleton
public class Controller {
    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    private Deployer deployer;
    private Invoker invoker;

    private boolean started;

    public Controller() {
    }

    /**
     * @param deployer a deployer that provides a list of services that will be controlled
     * @param invoker  an invoker that will be used to call @Start or @Stop method of each controlled service
     */
    @Inject
    public Controller(Deployer deployer, Invoker invoker) {
        this.deployer = deployer;
        this.invoker = invoker;
    }

    /**
     * Starts all services if they haven't been started yet.
     */
    public void startServices() {
        if (started) {
            log.warn("Services already started!");
            return;
        }
        started = true;
        List<Service> services = deployer.getServices();
        log.info("Starting {} services.", services.size());
        for (Service service : services) {
            log.debug("Starting {}", service);
            invoker.invokeStartAsync(service);
        }
    }

    /**
     * Stops all services if they are running.
     */
    public void stopServices() {
        if (!started) {
            log.warn("Services already stopped!");
            return;
        }
        started = false;
        List<Service> services = deployer.getServices();
        log.info("Stopping {} services.", services.size());
        for (Service service : services) {
            log.debug("Stopping {}", service);
            invoker.invokeStopAsync(service);
        }
    }

    /**
     * @return
     */
    @Lock(LockType.READ)
    public boolean isServicesStarted() {
        return started;
    }

    /**
     * Obtains an instance of Controller from BeanManager.
     *
     * @return a managed instance of Controller.
     * @throws IllegalStateException if CDI is not initialized yet
     */
    public static Controller getInstance() throws IllegalStateException {
        log.trace("Trying to obtain a Controller instance from BeanManager.");
        try {
            BeanManager beanManager = CDI.current().getBeanManager();
            Bean bean = beanManager.getBeans(Controller.class).iterator().next();
            CreationalContext creationalContext = beanManager.createCreationalContext(bean);
            Controller controller = (Controller) beanManager.getReference(bean, Controller.class, creationalContext);
            log.trace("Obtained {} from BeanManager.", controller);
            return controller;
        } catch (NullPointerException e) {
            throw new IllegalStateException("Could not get current BeanManager", e);
        }
    }

}