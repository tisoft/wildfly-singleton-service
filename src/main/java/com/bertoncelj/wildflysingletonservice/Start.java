package com.bertoncelj.wildflysingletonservice;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Marks a method on a bean as a service "start" method. This method will be called only on one server node (master) in a cluster.
 * If current master node goes down, this method will be called on another node (failover).
 *
 * @author Rok Bertoncelj
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface Start {
}
