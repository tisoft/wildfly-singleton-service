package com.bertoncelj.wildflysingletonservice;

import org.jboss.msc.service.ServiceActivator;
import org.jboss.msc.service.ServiceActivatorContext;
import org.jboss.msc.service.ServiceController;
import org.jboss.msc.service.ServiceRegistryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wildfly.clustering.singleton.SingletonServiceBuilderFactory;
import org.wildfly.clustering.singleton.SingletonServiceName;
import org.wildfly.clustering.singleton.election.SimpleSingletonElectionPolicy;

/**
 * @author Rok Bertoncelj
 */
public class WildflySingletonServiceActivator implements ServiceActivator {
    private static final String CONTAINER_NAME = "server";
    private static final Logger log = LoggerFactory.getLogger(WildflySingletonServiceActivator.class);

    @Override
    public void activate(ServiceActivatorContext context) throws ServiceRegistryException {
        log.info("Installing {}", WildflySingletonService.class.getSimpleName());
        WildflySingletonService wildflySingletonService = new WildflySingletonService();
        ServiceController<?> factoryService = context.getServiceRegistry().getRequiredService(SingletonServiceName.BUILDER.getServiceName(CONTAINER_NAME));
        SingletonServiceBuilderFactory factory = (SingletonServiceBuilderFactory) factoryService.getValue();
        factory.createSingletonServiceBuilder(WildflySingletonService.NAME, wildflySingletonService)
                .electionPolicy(new SimpleSingletonElectionPolicy())
                .build(context.getServiceTarget())
//                .addDependency(WeldStartService.SERVICE_NAME)
                .setInitialMode(ServiceController.Mode.ACTIVE)
                .install();
        log.debug("Installed {}", WildflySingletonService.class.getSimpleName());
    }
}
