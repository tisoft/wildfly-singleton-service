package com.bertoncelj.wildflysingletonservice;

import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Rok Bertoncelj
 */
public class WildflySingletonService implements org.jboss.msc.service.Service<String> {
    public static final ServiceName NAME = ServiceName.of("wildfly", "singleton", "service");
    private static final Logger log = LoggerFactory.getLogger(WildflySingletonService.class);

    private final AtomicBoolean started = new AtomicBoolean(false);

    @Override
    public void start(StartContext context) throws StartException {
        log.info("This node should now provide singleton services.");
        try {
            Controller.getInstance().startServices();
        } catch (IllegalStateException e) {
            log.debug("CDI not initialized yet. Services will be started later.");
            log.trace("Could not obtain an instance of Controller", e);
        }
    }

    @Override
    public void stop(StopContext context) {
        log.info("This node should not provide singleton services anymore.");
        Controller.getInstance().stopServices();
    }

    @Override
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        return System.getProperty("jboss.node.name");
    }


}
