package com.bertoncelj.wildflysingletonservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessBean;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * A CDI extension that looks for @Start-annotated methods in @ApplicationScoped or @Dependent CDI beans and also
 * in EJB @Singleton or @Stateless components. A {@link Service} instance is created for beans with such method and
 * stored in <code>services</code> list, accessible via {@link #getServices()} method.
 *
 * @author Rok Bertoncelj
 */
public class Deployer implements Extension {
    private static final Logger log = LoggerFactory.getLogger(Deployer.class);

    private List<Service> services = new ArrayList<>();

    public List<Service> getServices() {
        return services;
    }

    public void processBean(@Observes ProcessBean event) {
        Bean bean = event.getBean();
        if (!validateScope(bean.getScope())) {
            log.trace("Unsupported scope {} on bean: {}", bean.getScope().getSimpleName(), bean.getBeanClass().getSimpleName());
            return;
        }

        Method startMethod = null;
        Method stopMethod = null;
        for (Method method : bean.getBeanClass().getMethods()) {
            if (method.isAnnotationPresent(Start.class)) {
                log.debug("Found @Start {}.{}", bean.getBeanClass().getSimpleName(), method.getName());
                if (startMethod != null) {
                    throw new DeployerException("Multiple @Start methods found on bean %s",
                            bean.getBeanClass().getSimpleName());
                }
                startMethod = method;
            }
            if (method.isAnnotationPresent(Stop.class)) {
                log.debug("Found @Stop {}.{}", bean.getBeanClass().getSimpleName(), method.getName());
                if (stopMethod != null) {
                    throw new DeployerException("Multiple @Stop methods found on bean %s",
                            bean.getBeanClass().getSimpleName());
                }
                stopMethod = method;
            }
        }

        if (startMethod == null) {
            if (stopMethod != null) {
                throw new DeployerException("Missing @Start method on bean {}", bean.getBeanClass().getSimpleName());
            }
            log.trace("Bean is not a singleton service: {}", bean.getBeanClass().getName());
            return;
        }

        if (startMethod == stopMethod) {
            throw new DeployerException("Same method is used for both @Start and @Stop on bean %s",
                    bean.getBeanClass().getSimpleName());
        }

        Service service = new Service(bean, startMethod, stopMethod);
        services.add(service);
        log.info("Found {}", service);
    }

    private boolean validateScope(Class scope) {
        return scope.equals(ApplicationScoped.class) || scope.equals(Dependent.class);
    }


}
