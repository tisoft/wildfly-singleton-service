package com.bertoncelj.wildflysingletonservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import java.lang.reflect.Method;

/**
 * Invokes @Start and @Stop methods of services. EJB's @Asynchronous functionality is used to invoke those methods
 * asynchronously to avoid blocking in {@link Controller#startServices()} and {@link Controller#stopServices()} methods.
 *
 * @author Rok Bertoncelj
 */
@Stateless
public class Invoker {
    private static final Logger log = LoggerFactory.getLogger(Invoker.class);

    /**
     * Asynchronously invokes a @Start method of specified service.
     *
     * @param service
     */
    @Asynchronous
    public void invokeStartAsync(Service service) {
        invokeMethod(service.getBean(), service.getStartMethod());
    }

    /**
     * Asynchronously invokes a @Stop method of specified service
     *
     * @param service
     */
    @Asynchronous
    public void invokeStopAsync(Service service) {
        invokeMethod(service.getBean(), service.getStopMethod());
    }

    private void invokeMethod(Bean bean, Method method) {
        log.trace("Invoking {}.{}", bean.getBeanClass().getSimpleName(), method.getName());
        BeanManager beanManager = CDI.current().getBeanManager();
        CreationalContext ctx = beanManager.createCreationalContext(bean);
        Object beanInstance = beanManager.getReference(bean, bean.getBeanClass(), ctx);
        try {
            method.invoke(beanInstance);
        } catch (Exception e) {
            log.error(String.format("Could not call %s.%s", bean.getBeanClass().getSimpleName(), bean.getName()), e);
        }
    }
}
