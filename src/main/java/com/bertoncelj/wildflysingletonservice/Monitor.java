package com.bertoncelj.wildflysingletonservice;

import org.jboss.as.server.CurrentServiceContainer;
import org.jboss.msc.service.ServiceContainer;
import org.jboss.msc.service.ServiceController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Continuously checks if local server node is currently a master that should run the services and starts them if necessary.
 * Check is performed at application startup and then every 5 seconds as a sort of backup mechanism if something
 * goes wrong during startup or during promotion to master.
 *
 * @author Rok Bertoncelj
 */
@Singleton
@Startup
public class Monitor {
    private static final Logger log = LoggerFactory.getLogger(Monitor.class);

    @PostConstruct
    public void startupCheck() {
        log.trace("Performing startup check.");
        try {
            check();
        } catch (IllegalStateException e) {
            log.warn("Startup check failed - service is probably not ready yet: {}", e.getMessage());
        }
    }

    @Schedule(hour = "*", minute = "*", second = "*/5", persistent = false)
    public void periodicCheck() {
        log.trace("Performing periodic check.");
        try {
            check();
        } catch (IllegalStateException e) {
            log.warn("Periodic check failed - service is probably not ready yet: {}", e.getMessage());
        }
    }

    /**
     * Checks if local server node is currently a master that should run the services and starts them if necessary.
     */
    public void check() {
        ServiceContainer serviceContainer = CurrentServiceContainer.getServiceContainer();
        ServiceController serviceController = serviceContainer.getService(WildflySingletonService.NAME);
        String nodeName = (String) serviceController.getValue();
        Controller controller = Controller.getInstance();
        if (nodeName.equals(System.getProperty("jboss.node.name")) && !controller.isServicesStarted()) {
            log.debug("Local node is master, but services were not started. Starting them now.");
            controller.startServices();
        }
    }
}
